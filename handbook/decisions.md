# Decision making process

The exact application may vary depending on the context, but the general pattern for taking
decisions at OpenCraft should all follow the same base format:

* Is this a decision that only affects you, or others? If it only affects you, take the decision,
  and if you think anyone could be interested, let them know about it. Err on the side of over-
  communicating on this, there is little downside to it, but a lot of benefits.
* If it affects others besides you, they will need to review it. Prepare a proposal:
    * It's often better to start with the problem and ask for advice or potential solutions
      from those with expertise or who would be affected.
    * Then prepare the proposal itself, e.g. a pull request for code, or for changes to the
      handbook's rules;
    * For a trivial decision, a comment in one of OpenCraft's tools (if it happens on a
      synchronous tool like the chat though, make sure to document this discussion and link to it
      from an asynchronous tool);
    * For any other matter, describe the decision to take and the options in a Google Doc on the
      shared folder, and post it on an asynchronous discussion tool, such as the forum or Jira,
      pinging anyone who you think could be affected by or interested in the change. Also err on
      the side of over-communicating on this.
* Participate in the conversation/review if there are comments. Try to address them, until none
  of the outstanding comments are blocking for the reviewers. The goal isn't full consensus - the
  reviewers should be mindful of encouraging action, and it is an acceptable result to only address
  comments up to the point where the reviewer can live with it.
* If there is disagreement between the proposer and the reviewers, a manager decides: the manager
  involved in the review or proposal if there is one; if there isn't one, the CTO for technical
  decisions or the CEO for other decisions. At any point and for any topic, the CEO can take a
  final decision, and/or stop the discussion; when this happens the CEO explicitly indicates that
  the use of his "[BDFL](https://en.wikipedia.org/wiki/Benevolent_dictator_for_life)" powers, to
  allow to differentiate between a discussion/argumentation and a decision.
* If there is agreement - which is hoped to be the case for the very vast majority of decisions -
  then the decision is taken. To make it so, state the outcome explicitly in the discussion thread,
  or merge the PR if there is one.
