# Recruitment Email Template

Hi XXX,

I was glad to see your contributions! What you have shown me of you (and of the work you have done) interested me, and I'd like to meet you.

The meeting itself would be in Zoom, and should be fairly short (around 30 minutes) -- it's a first contact. To make it easier to pick a time that works for both of us, I'm using a tool. Could you book a time there?

https://CALENDLY.URL/

Btw, if you aren't already using Zoom regularly, make sure you have the plugin installed: https://support.zoom.us/hc/en-us/articles/207373866-Zoom-Installers, test it out with a friend or another computer, and have headsets ready and a functioning microphone. It seems silly to say, but that's actually a frequent problem : )

Also, can you have a development editor ready, for some live coding? You will want to make sure you can share your screen using Zoom, and make sure the editor font is big enough to be readable through screen sharing. (And don't stress out too much about this, it will be short and simple questions.)

I would also like to record the meeting, to be able to share it with the team afterwards - let me know if you have any issue with this though!

Looking forward to the discussion : )

YOUR-NAME
@OpenCraft
