# Rejection Recruitment Email Template

Hi XXX,

Thank you again for meeting with me - it was a pleasure to talk to you.

We have just finished reviewing the candidatures; I'm sad to say that we will not be retaining your application for the next round. Please do not take it personally - there were a lot of strong profiles, and we do make a lot of mistakes.

In any case, I wish you good luck in your job search, and I hope you end up finding a good position!

YOURNAME
@OpenCraft
