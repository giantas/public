# Free Software

## Choosing the project privacy

Most free software projects should be public from the start, especially if they are under an open-source license or meant to be released under one in the future.

If the repository contains anything that shouldn't be visible to the public eye, then that content should be better isolated from the project in order to keep the project publicly available.

## Choosing a repository management service

### GitLab

GitLab is open sourced. Meanwhile, GitHub is closed and proprietary. Therefore, most projects created should be on GitLab.

However, creating the repository on GitLab is a bit tricky, since there's a lot of different of different groups inside some organizations. Comparing the project with the group names will provide a good idea where it best belongs.

For example, if the project belongs to a client, then it should be under `client/<client name>/<project name>`. If a project is documentation repository or learning content, then it should be under `documentation/<project name>`. Meanwhile, if a project is a tool developed by OpenCraft for OpenCraft, then `dev/<project name>` is the right place for it.

### GitHub

When projects contain code that is meant to be merged into one of the projects from the edX GitHub organization, then they should be created on GitHub.

After creating your repository, you'll want to make sure that the members in your organization have the necessary permissions to make changes to the repository. In order to make sure your team members have the necessary privileges, follow the following steps.

1. Open the repository.
1. Go to the repository settings.
1. Go to "Manage access" on the left sidebar.
1. Click on "invite teams or people".
1. Invite `opencraft-dev`.
1. Choose the *Admin* role.

## Running a Free Software project

An outside contributor is someone who proposes a change to a Free
Software repository for which OpenCraft is the maintainer. They may
fix or report a bug, propose or implement a new feature. OpenCraft
welcomes and encourages such contributions, even though they may be
unplanned and outside the roadmap. The [How to Run a Successful Free
Software Project](https://producingoss.com/) book covers everything
there is to know and this section is only about OpenCraft specifics:

* If, by chance, the contribution exactly matches an existing Xh
  ticket scheduled in the current sprint and not started, it
  essentially is an Xh gift to OpenCraft and is therefore worth Xh of
  review time. Because if it was not for the contribution an OpenCraft
  team member would have to do it and spend Xh anyways.

* If that is not the case, for each contribution a dedicated ticket
  timeboxed to 1h can be created in the relevant cell, under the
  Contributions Epic.

* If a firefight has time and is willing to, it is good to reply as
  soon as possible and schedule the ticket either to the upcoming
  sprint or to the current one if a firefighter has time, and inform
  the contributor of when to expect the review.
