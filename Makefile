# Makefile for OpenCraft handbook.
all: install_prereqs quality compile

compile:
	mkdocs build

install_prereqs:
	pip install -r requirements.txt

quality:
	make quality-python
	make quality-markdown

quality-python:
	pylint hooks
	mypy hooks

quality-markdown:
	npx markdownlint-cli handbook

run:
	mkdocs serve

clean:
	rm -rvf build
