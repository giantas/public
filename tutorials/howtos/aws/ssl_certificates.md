This guide contains an overview of how to handle adding/replacing an SSL certificate given to us by a client in AWS.

1. Request the following information from the client:
    * Certificate
    * Private Key
    * Certificate Chain (needed if certificate is not self signed)
2. Import the certificate
    1. Switch to the Role & Region specific to the Client
    2. Open AWS Certificate Manager
        * If an imported Certificate doesn't exist yet:
            1. Import Certificate
               ![Import certificate screen shot](../../images/import-certificate-in-certificate-manager.png) 
            2. Enter the following in the corresponding fields:
                * Certificate file content -> Certificate body field
                * Private key file content -> Private key field
                * Certificate chain file content -> Certificate chain field
            3. Press next to enter any tags you might want to enter
            4. Press Review & Import to import the certificate
        * If an imported Certificate already exists:
            1. Expand the certificate
            2. Press the "Re-import certificate" button
               ![Screenshot of using the re-import button](../../images/certificate-reimport.png)
            3. Follow the previous steps to import a certificate
4. If related resources are managed with terraform:
    1. Find the terraform config for this client.
    2. Find all references to the old certificate ARN and update them to the new ARN.
    3. Follow the standard process to update and apply the terraform for the client (may include PRs, reviews, checking terraform plan output before applying, etc.)
3. Check which resources are still using the old certificate
    1. Expand the certificate
    2. See the "Associated Resources" field
       ![Screenshot with the associated resources field](../../images/certificate-associated-resources.png) 
4. Use the certificate on the Load Balancers and other Resources (skip if managed with terraform)
    1. Open EC2 in AWS
    2. Navigate to the Load Balancers
    3. Choose the Load Balancer that is listed among the Associated Resources of the previous certificate
    4. Navigate to the Listeners tab to change the SSL certificate
       ![Screenshot of the load balancer lister tab](../../images/loadbalancer-listener-tab.png)
    5. Choose the Certificate you just imported to AWS Certificate Manager
       ![Screenshot of certificate selection](../../images/selecting-certificate-from-aws-certificate-manager.png)

