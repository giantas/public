# Elasticsearch + memcached VM

This document describes how to set up an OVH VM which can be used by an Ocim instance to persist Elasticsearch and
memcached services across Ocim appserver deployments.

To set up Elasticsearch or memcached on AWS, see the [AWS ELB deployment tutorial](howtos//aws/AWS_ELB_deployment_tutorial.md).

## Security Groups

Because Open edX doesn't support securing Elasticsearch or memcached by password or other means, we need to use OVH
security groups in order to allow access to this new VM from only the specific Ocim instance's appservers.

Since these security groups need to reference each other and the Ocim and Elasticsearch VMs, they need to live in the
OVH OpenCraft IM - Production - VMs GRA1 region
([vault credentials](https://vault.opencraft.com:8200/ui/vault/secrets/secret/show/core/OpenCraft%20Tools%20:%20Resources%20:%20OpenStack%20:%20OVH%20-%20OpenStack%20-%20Horizon%20-%20OpenCraft%20IM%20-%20Production%20-%20VMs)).

Under Networks, add 2 new security groups using the client's name.

* `clientname-edxapp`: contains no rules.
* `clientname-elasticsearch`: allow the following access:

   |Direction|Ether Type|Protocol|Port Range |Remote IP Prefix|Remote Security Group   |
   |---------|----------|--------|-----------|----------------|------------------------|
   |Egress   |IPv4      |Any     |Any        |0.0.0.0/0       |-                       |
   |Egress   |IPv6      |Any     |Any        |::/0            |-                       |
   |Ingress  |IPv4      |TCP     |22 (SSH)   |0.0.0.0/0       |-                       |
   |Ingress  |IPv4      |TCP     |53 (DNS)   |0.0.0.0/0       |-                       |
   |Ingress  |IPv4      |TCP     |80 (HTTP)  |0.0.0.0/0       |-                       |
   |Ingress  |IPv4      |TCP     |19100      |0.0.0.0/0       |-                       |
   |Ingress  |IPv4      |TCP     |9200 - 9300|-               |clientname-edxapp       |
   |Ingress  |IPv4      |TCP     |9200 - 9300|-               |clientname-elasticsearch|
   |Ingress  |IPv4      |TCP     |11211      |-               |clientname-edxapp       |

## OVH VM

Create a new VM on in the same GRA1 region as the security groups.  See [Setting up new
servers](howtos/setting-up-new-servers.md) for details.  This VM does not need database access or SMTP, and details for
provisioning are below.

Ensure that the `clientname-elasticsearch` security group is the only one on the new VM, and ensure that the `default`
security group is removed.

### Provisioning

See [ansible-secrets#176](https://github.com/open-craft/ansible-secrets/pull/176/) for an example.

* Create `host` entries in the `elasticsearch-prod:children` and `memcached` groups. 
* Add a `host_vars` file and set up like the other elasticsearch instances.
* Create a new Dead Man's Snitch ([vault credentials](https://vault.opencraft.com:8200/ui/vault/secrets/secret/show/core/OpenCraft%20Tools%20:%20Resources%20:%20Dead%20Man's%20Snitch)),
  tagged with `sanity-checks`, `elasticsearch` and the client's name.  Configure it as a Basic alert, checked every 15
  minutes, and alert `urgent@opencraft.com` and `ops@opencraft.com` on error.

  Use the Unique Snitch URL from the Setup tab in the `SANITY_CHECK_SNITCH` variable.

Provision the VM as per the [ansible-secrets README](https://github.com/open-craft/ansible-secrets#getting-started).

Once the provisioning is complete, check that the new snitch is online, and that your new host shows up OK the
[prometheus targets](https://prometheus.net.opencraft.hosting/targets) list.

## Troubleshooting

Check that all of these services are Active/Enabled, and show no errors in the logs.

```bash
# Sometimes, Elasticsearch shows errors in the logs even if the service is Active/Enabled.
# Restart it if you see any, and verify they are resolved.
sudo systemctl status elasticsearch

# memcached should be running
# Restart it if you have any issues connecting from the appserver.
sudo systemctl status memcached

# node exporter service should be running.  Prometheus will show a 502 Bad Gateway if it isn't.
sudo systemctl status nodeexporter
```

If the ansible playbook completed successfully but you're not seeing these services running, then it could be an issue
with the ansible-playbook submodules. Read the [Getting started](https://github.com/open-craft/ansible-secrets#getting-started)
instructions again and re-run ansible.

## Update Ocim Instance 

Configure the Ocim instance to use the new VM for Elasticsearch and memcached.  At time of writing, this means adding
the following to the Configuration Extra Settings:

```yaml
ELASTICSEARCH_HOST: elasticsearch-clientname.net.opencraft.hosting
ELASTICSEARCH_PORT: 9200
EDXAPP_ELASTIC_SEARCH_CONFIG:
  - host: "{{ ELASTICSEARCH_HOST }}"
    port: 9200  # must be int
FORUM_ELASTICSEARCH_HOST: "{{ ELASTICSEARCH_HOST }}"
FORUM_ELASTICSEARCH_PORT: "{{ ELASTICSEARCH_PORT }}"
DISCOVERY_ELASTICSEARCH_URL: "http://{{ ELASTICSEARCH_HOST }}:{{ ELASTICSEARCH_PORT }}/"
EDX_NOTES_API_ELASTICSEARCH_URL: "http://{{ ELASTICSEARCH_HOST }}:{{ ELASTICSEARCH_PORT }}/"
EDXAPP_MEMCACHE: [ "{{ ELASTICSEARCH_HOST }}:11211" ]
```

Attach the `clientname-edxapp` security group to the Ocim instance by adding it to the "Additional security groups" field.

If you are running ecommerce and course discovery on the instance, see [Configuring Ecommerce and Course
Discovery](https://ocim.opencraft.com/en/latest/howtos/) for instance configuration, and steps to run after the
new appserver is deployed.

Spawn a new appserver.
