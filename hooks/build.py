"""
Build hooks used by mkdocs for automating some portions of our handbook.
"""
from itertools import chain
from pathlib import Path
from typing import Callable, Mapping

import yaml

from jinja2 import Environment, FileSystemLoader
from typeguard import typechecked

from hooks.constants import (
    TEMPLATES_PATH, DATA_PATH, TEAM_FILE_PATH, CELL_TEMPLATE, CELL_OUTPUT_PATH,
)
from hooks.schema import OpenCraft


@typechecked
def load_raw_team(path: Path) -> OpenCraft:
    """
    Loads information about the team, validating the YAML schema as it returns. If the dictionary that results from
    loading the YAML does not match expectations, this will throw and give a hint as to where in the schema the error
    was.
    """
    with open(path, 'r') as source_file:
        return yaml.load(source_file)


def load_team(path: Path) -> OpenCraft:
    """
    Loads the team from a YAML file and then applies checks and transformations.
    """
    team = load_raw_team(path)
    for cell in chain(team['cells'], (team['meta_cell'],)):
        members = {entry['email'].lower(): entry['name'] for entry in cell['members']}
        for role, email in cell['roles'].items():
            if not email:
                continue
            email = email.lower()
            # Validate cell membership, but only if this is a regular cell.  (Regular cell members may still take
            # company-wide roles such as Community Liaison.)
            if 'name' in cell and email not in members:
                raise ValueError(f'Could not find cell member with email {email} in {cell["name"]}! (For role: {role})')
            # No way to do 'KeyOf'?
            cell['roles'][role] = email
    return team


def render_template(*, context: Mapping, template: str) -> str:
    """
    Given a dictionary, and a path to a template file, renders out a template to string.
    """
    env = Environment(
        trim_blocks=True,
        lstrip_blocks=True,
        keep_trailing_newline=True,
        loader=FileSystemLoader([str(TEMPLATES_PATH)]),
    )
    compiled_template = env.get_template(template)
    return compiled_template.render(**context)


def update_cells_page(team_file: Path, cell_template: str, output_path: Path) -> None:
    """
    Compiles team data from a YAML file and then renders it out to markdown template.
    """
    team = load_team(team_file)

    output = render_template(context=team, template=cell_template)
    try:
        with open(output_path, 'r') as original_file:
            existing = original_file.read()
    except IOError:
        existing = ""
    if output == existing:
        # Exit early, or else we'll trigger a watch loop, since only write time is checked by the watcher.
        return
    with open(str(output_path), 'w') as output_file:
        output_file.write(output)


def update_templated(*_args, **_kwargs):
    """
    Prebuild hook to be called before each build. Referenced in mkdocs.yml.
    """
    update_cells_page(TEAM_FILE_PATH, CELL_TEMPLATE, CELL_OUTPUT_PATH)


def include_hook_files(server, config: dict, builder: Callable):  # pylint: disable=unused-argument
    """
    Hook run when mkdocs serve initial starts up. Using this, we can add files/directories to the watcher.
    We need to do this in order to make sure that updating this module, or the data it uses, results in those changes
    being updated.

    Note: Watching python modules used in hooks doesn't work. Once imported, they are not reloaded. You'll have
    to restart the serve process for changes in this file to be recognized.
    """
    server.watch(str(TEMPLATES_PATH), builder)
    server.watch(str(DATA_PATH), builder)
